# monkey

CLI komutlarınızı argüman verileri ile birlikte yaml dosyası üzerinden yönetmenizi sağlar.

## Kurulum
```
pip install -r requirements.txt
```

## Çalıştırma

Konfügrasyon için yaml dosyası oluşturunuz. Bkz. (Konfügrasyon dosyası oluşturma)

Aşağıdaki sözdizimi ile bir komutu çalıştırabilirsiniz.
```bash
python monkey.py <konfügrasyon dosyası yolu> <script> [-p <değişken yolu>]
```

Örnek bir komut:
```bash
python monkey.py conf.yaml hello
```

### Özel scriptler
`echo` ve `keys` adında iki özel script vardır. `echo` belirtilen değişkenin değerini görüntüler. `keys` ise belirtilen değişkeni iteratif bir şekilde görüntüler.

Örneğin tüm scriptleri görüntülemek için:
```
python monkey.py conf.yaml keys -p scripts
```

## Konfügrasyon dosyası oluşturma

```yaml
scripts:
    hello: # script adı
        - echo 'Hello World!' # çalıştırılacak cli komutu
    hello2:
        - echo 'Hello {{root.person.name}}!' # jinja template kullanabilirsiniz.
    write:
        - echo '{{data | json }}' > output.json

person:
    name: Joe Doe
```

Yukarıdaki ``hello`` scriptini çalıştırmak için

```
python monkey.py conf.yaml hello
```
Çıktı:
> Hello World!

```
python monkey.py conf.yaml hello2
```
Çıktı:
> Hello Joe Doe!

```
python monkey.py conf.yaml write -p person
```
`output.json` dosyası:
```json
{
    "name": "Joe Doe"
}
```

> **NOT:** cli üzerinden girilen -p parametresindeki değişken yolunun verisi için `{{data}}`, doğrudan yaml dosyasındaki bir veriye ulaşmak için `{{root}}` kullanınız.
